function getData() {
    return $.get("data/index.json");
}

getData().then((data) => {
    data.forEach(element => {
        displayData(element);
    });
});


function displayData(data) {

    let cell1 = $("<div>").addClass("col-sm-4").append(data.name);
    let cell2 = $("<div>").addClass("col-sm-4").append(data.grade);
    let cell3 = $("<div>").addClass("col-sm-4").append(data.status);

    let row = $("<a>").addClass("row p-3 mb-2")
        .append(cell1)
        .append(cell2)
        .append(cell3)
        .css("margin-bottom", "10px");

    if (data.status == "done") {
        row.addClass("bg-success")
            .addClass("text-white");
    }
    else if (data.status == "fix") {
        row.addClass("bg-danger")
            .addClass("text-white");
    }
    else {
        row.addClass("bg-warning")
            .addClass("text-white");
    }

    $('#dataContainer').append(row);
}